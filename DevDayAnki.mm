<map version="freeplane 1.6.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Anki, oubliez d&apos;oublier" FOLDED="false" ID="ID_383088669" CREATED="1504612847336" MODIFIED="1507145422278" LINK="../biblio%20Doc%20&#xe9;crite/Carte%20des%20cartes.mm">
<edge STYLE="bezier" WIDTH="thin"/>
<hook NAME="MapStyle">
    <properties fit_to_viewport="false;" show_icon_for_attributes="true" show_note_icons="true" show_notes_in_map="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600.0 px" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node POSITION="right" ID="ID_1301104993" CREATED="1506506914784" MODIFIED="1507145283965"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="Anki-icon.svg.png" height="200" width="200"/>
      
    </p>
    <p>
      Pr&#233;sentation au dev day, Rennes 2017.
    </p>
    <p>
      nicolas.bossard@orange.com
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="L&apos;oubli..." FOLDED="true" POSITION="right" ID="ID_1737825275" CREATED="1506506368003" MODIFIED="1506506373722">
<node TEXT="nous oublions en fait environ 75 % des notions apprises 48 heures apr&#xe8;s l&#x2019;apprentissage" ID="ID_1742780470" CREATED="1504614964073" MODIFIED="1504614977816">
<font ITALIC="true"/>
<node ID="ID_429726391" CREATED="1506288756590" MODIFIED="1506288756590" LINK="https://fr.wikipedia.org/wiki/Hermann_Ebbinghaus"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    1885, <a href="https://fr.wikipedia.org/wiki/Hermann_Ebbinghaus" title="Hermann Ebbinghaus">Hermann Ebbinghaus</a>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="image courbe de l&apos;oubli" ID="ID_461757672" CREATED="1506288762307" MODIFIED="1506288778570">
<node TEXT="http://business.agorize.com/wp-content/uploads/2017/01/Courbe-de-loubli-dEbbinghaus-la-limite-du-COOC-pour-sa-transformation-digitale-Agorize-le-blog.jpg" ID="ID_1134912099" CREATED="1506501644176" MODIFIED="1506502016432" LINK="http://business.agorize.com/wp-content/uploads/2017/01/Courbe-de-loubli-dEbbinghaus-la-limite-du-COOC-pour-sa-transformation-digitale-Agorize-le-blog.jpg">
<attribute NAME="slide-hide" VALUE=""/>
</node>
<node ID="ID_138730996" CREATED="1506501780581" MODIFIED="1506506675480"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="./Courbe-de-loubli-dEbbinghaus-la-limite-du-COOC-pour-sa-transformation-digitale-Agorize-le-blog.jpg"/>
      
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="image courbe de l&apos;oubli avec r&#xe9;p&#xe9;tition" FOLDED="true" ID="ID_811021920" CREATED="1506288779517" MODIFIED="1506288856184">
<node TEXT="https://www.mondelangues.fr/wp-content/uploads/2015/11/courbe-de-loubli.jpg" ID="ID_192392009" CREATED="1506288857834" MODIFIED="1506502145122" LINK="https://www.mondelangues.fr/wp-content/uploads/2015/11/courbe-de-loubli.jpg">
<attribute NAME="slide-hide" VALUE=""/>
</node>
<node ID="ID_1924241307" CREATED="1506502090237" MODIFIED="1506506203320"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="courbe-de-loubli.jpg"/>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="Anki, la solution" POSITION="right" ID="ID_1653786620" CREATED="1506506396890" MODIFIED="1506506406212">
<node ID="ID_788306918" CREATED="1506284745928" MODIFIED="1507147895674"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    Initialement cr&#233;&#233; en 2012 pour apprendre le <strong>japonais</strong>&#160;(Anki veut dire &#8220;m&#233;moriser&#8221; en japonais)
  </body>
</html>

</richcontent>
</node>
<node TEXT="quid" ID="ID_1433672420" CREATED="1504613208743" MODIFIED="1504613212385">
<node TEXT="un logiciel de r&#xe9;p&#xe9;tition espac&#xe9;e de fiches... tr&#xe8;s performant" ID="ID_254115072" CREATED="1504613213142" MODIFIED="1504615652129"/>
</node>
<node TEXT="objectifs" ID="ID_1164177527" CREATED="1506283774946" MODIFIED="1506283781632">
<node TEXT="les fiches de r&#xe9;vision on connait tous mais..." ID="ID_1591270337" CREATED="1504615290857" MODIFIED="1504615314459"/>
<node TEXT="il ne s&apos;agit plus la d&apos;&#xea;tre pr&#xea;t pour un examen, puis les archiver d&#xe9;finitivement, mais de retenir pour des ann&#xe9;es" ID="ID_1176248633" CREATED="1504615315111" MODIFIED="1504615676312"/>
<node TEXT="il faut pouvoir tenir la mont&#xe9;e en charge, et donc&#xa;- r&#xe9;viser rapidement, partout&#xa;- ne r&#xe9;viser que ce qu&apos;on est susceptible d&apos;avoir oubli&#xe9;" ID="ID_255043182" CREATED="1504615346037" MODIFIED="1506503009367"/>
</node>
<node TEXT="principe de base" ID="ID_53747487" CREATED="1504615276370" MODIFIED="1504615279844">
<node TEXT="2 principes de base :" ID="ID_1578086277" CREATED="1506503194690" MODIFIED="1506503210708"/>
<node TEXT="r&#xe9;vision active" ID="ID_330066640" CREATED="1506283786896" MODIFIED="1506503172362">
<node TEXT="se poser des questions, plutot que simplement relire" ID="ID_741521500" CREATED="1506503173586" MODIFIED="1506503176821"/>
</node>
<node TEXT="r&#xe9;p&#xe9;titions espac&#xe9;es" ID="ID_1849540550" CREATED="1506283844255" MODIFIED="1506283850194">
<node TEXT="faire r&#xe9;apparaitre la fiche seulement quand vous risquez de l&apos;avoir oubli&#xe9;" ID="ID_1375914524" CREATED="1506283850655" MODIFIED="1506283876201"/>
</node>
</node>
<node TEXT="screenshots" ID="ID_1880680120" CREATED="1504617763684" MODIFIED="1504617767721">
<node TEXT="pr&#xe9;sence sur tous OS et tous terminaux" ID="ID_273235797" CREATED="1507128531431" MODIFIED="1507128544631"/>
<node TEXT="windows / mac /linux" ID="ID_1487993036" CREATED="1504617781221" MODIFIED="1507044644975">
<node TEXT="principaux &#xe9;crans :" ID="ID_1385511963" CREATED="1507043158324" MODIFIED="1507147919461"/>
<node TEXT="home" FOLDED="true" ID="ID_662613365" CREATED="1507029262638" MODIFIED="1507029446103">
<node ID="ID_300261538" CREATED="1507029292189" MODIFIED="1507035256643"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="./windows_home.png" height="512" width="478"/>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="paquet" FOLDED="true" ID="ID_655357978" CREATED="1507128647348" MODIFIED="1507128649853">
<node LOCALIZED_STYLE_REF="default" ID="ID_420629492" CREATED="1507029292189" MODIFIED="1507142720415"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="./windows_paquet.png"/>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="flashcard" FOLDED="true" ID="ID_1663869702" CREATED="1507029448045" MODIFIED="1507035090455">
<node LOCALIZED_STYLE_REF="default" ID="ID_859782424" CREATED="1507029292189" MODIFIED="1507128774135"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="./windows_screenshot.png" height="400" width="377"/>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="graph" FOLDED="true" ID="ID_1071943642" CREATED="1507029448045" MODIFIED="1507029450318">
<node ID="ID_1437258922" CREATED="1507029292189" MODIFIED="1507034953577"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="./windows_graph.png"/>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="android" ID="ID_97919285" CREATED="1504617782949" MODIFIED="1507288899598">
<node ID="ID_641115461" CREATED="1507043558273" MODIFIED="1507043574998"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="./ankidroid.png"/>
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="widget" ID="ID_1099614579" CREATED="1504617788797" MODIFIED="1504617790225"/>
</node>
<node TEXT="iOS" ID="ID_231311952" CREATED="1507288900687" MODIFIED="1507288903512">
<node ID="ID_930016815" CREATED="1507289008374" MODIFIED="1507289131890"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="ios.jpg" width="196" height="348"/>
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="28 euros pour la version iOS" ID="ID_1657749335" CREATED="1507288904271" MODIFIED="1507288916849"/>
</node>
<node TEXT="wear" FOLDED="true" ID="ID_1305993009" CREATED="1504617784644" MODIFIED="1504617787441">
<node ID="ID_1235825651" CREATED="1507043427082" MODIFIED="1507043511339"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="./ankiwear_list_of apps.png"/>
      <img src="./ankiwear_question.png"/>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="web" ID="ID_979746915" CREATED="1507029245213" MODIFIED="1507029246663">
<node TEXT="principaux &#xe9;crans :" ID="ID_1719302733" CREATED="1507147899182" MODIFIED="1507147928519"/>
<node TEXT="home" FOLDED="true" ID="ID_1612655066" CREATED="1507044557694" MODIFIED="1507044559503">
<node ID="ID_973890630" CREATED="1507044366832" MODIFIED="1507044386874"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="./web_list.png"/>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="paquet" FOLDED="true" ID="ID_133477255" CREATED="1507044567886" MODIFIED="1507044569767">
<node ID="ID_1800077020" CREATED="1507044402222" MODIFIED="1507044450491"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="./web_paquet.png"/>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="flashcard" FOLDED="true" ID="ID_93514297" CREATED="1507044572062" MODIFIED="1507044574744">
<node ID="ID_1757943623" CREATED="1507044452799" MODIFIED="1507044467768"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="./web_flashcard.png"/>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node TEXT="exemple d&apos;utilisation" ID="ID_1358471890" CREATED="1504614231250" MODIFIED="1504614291615">
<node TEXT="vous &#xe9;crivez des fiches question r&#xe9;ponse sur ce que vous aimeriez retenir... ou approfondir" ID="ID_372498023" CREATED="1504614255027" MODIFIED="1504614328120"/>
<node TEXT="pendant une conf, une formation, quand vous lisez un article..." ID="ID_1334858032" CREATED="1504614295965" MODIFIED="1504614371977"/>
<node TEXT="la fiche va revenir r&#xe9;guli&#xe8;rement, vous allez pouvoir l&apos;am&#xe9;liorer, puis l&apos;apprendre" ID="ID_235385167" CREATED="1504614372662" MODIFIED="1504614401882"/>
<node TEXT="sa fr&#xe9;quence de r&#xe9;apparition est bas&#xe9;e sur l&apos;algorithme SM2" ID="ID_92865290" CREATED="1504614404775" MODIFIED="1507023471518">
<node TEXT="algorithme Super Memo 2, prend en compte :" ID="ID_1051975043" CREATED="1504614404775" MODIFIED="1507023570286"/>
<node TEXT="facilit&#xe9; d&#xe9;clar&#xe9;e &#xe0; y r&#xe9;pondre" ID="ID_503957792" CREATED="1504614444456" MODIFIED="1504614457900"/>
<node TEXT="vitesse de r&#xe9;ponse" ID="ID_463705708" CREATED="1504614459393" MODIFIED="1504614464772"/>
<node TEXT="espacement pr&#xe9;c&#xe9;dent" ID="ID_858544029" CREATED="1504614465024" MODIFIED="1504618279335"/>
<node TEXT="SM2 : https://www.supermemo.com/english/ol/sm2.htm" ID="ID_1246819378" CREATED="1504618421503" MODIFIED="1507023629526" LINK="https://www.supermemo.com/english/ol/sm2.htm"/>
</node>
<node TEXT="ainsi une fiche va apparaitre les premiers jours puis rapidement sa fr&#xe9;quence d&apos;apparition va tomber &#xe0; qques mois :&#xa;... si vous avez une bonne m&#xe9;moire" ID="ID_764508020" CREATED="1504614479985" MODIFIED="1507023680535"/>
</node>
<node TEXT="Un tr&#xe8;s bon logiciel" ID="ID_1031844253" CREATED="1504613248124" MODIFIED="1507148268950">
<node TEXT="nombreuses qualit&#xe9;s :" ID="ID_972302875" CREATED="1507148270067" MODIFIED="1507148274876"/>
<node TEXT="contenu des cartes potentiellement vari&#xe9; :&#xa;audio, image, vid&#xe9;o, texte &#xe0; trous" ID="ID_1917539349" CREATED="1504614692393" MODIFIED="1507148221243"/>
<node TEXT="r&#xe9;ellement multi-plateforme" ID="ID_380604937" CREATED="1504613000357" MODIFIED="1507044648905">
<node TEXT="comme vu pr&#xe9;c&#xe9;demment" ID="ID_419422790" CREATED="1507044661429" MODIFIED="1507044682405">
<attribute NAME="slide-hide" VALUE=""/>
</node>
</node>
<node TEXT="synchronisation tr&#xe8;s efficace, bonne gestion des conflits" ID="ID_477269238" CREATED="1504613011141" MODIFIED="1504613106945"/>
<node TEXT="tr&#xe8;s stable et rapide, m&#xea;me si on le charge" ID="ID_1297633176" CREATED="1504614644582" MODIFIED="1504614801069"/>
<node TEXT="open source&#xa;(https://github.com/dae/anki https://github.com/ankidroid/Anki-Android)" ID="ID_1067803466" CREATED="1504613264186" MODIFIED="1507148225324"><richcontent TYPE="NOTE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Sauf la version iOS
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="bien document&#xe9;" ID="ID_1057330721" CREATED="1504615845301" MODIFIED="1504615849499"/>
<node TEXT="en &#xe9;volution" ID="ID_1655264515" CREATED="1504613269306" MODIFIED="1504613276557"/>
<node TEXT="plateforme gratuite&#xa;(serveurs pay&#xe9;s par version iOS)" ID="ID_1265429711" CREATED="1504613435398" MODIFIED="1507044775317"/>
<node TEXT="aspect communautaire" ID="ID_1944476033" CREATED="1506506535849" MODIFIED="1506506541018">
<node TEXT="possiblit&#xe9; de r&#xe9;cup&#xe9;rer/partager des jeux de fiches sur un sujet" ID="ID_1916726390" CREATED="1504614711336" MODIFIED="1504614736212"/>
<node TEXT="liste th&#xe9;matique" FOLDED="true" ID="ID_910480648" CREATED="1507235400242" MODIFIED="1507235407243">
<node ID="ID_1728489872" CREATED="1507208244261" MODIFIED="1507235265013"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="getdeck_full_list.png"/>
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="syst&#xe8;me de notation &#xe0; &#xe9;toiles" FOLDED="true" ID="ID_1927475495" CREATED="1507235386434" MODIFIED="1507235397852">
<node ID="ID_1466217896" CREATED="1507208244261" MODIFIED="1507235286950"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="getdeck_sub_list.png"/>
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="preview / multimedia" FOLDED="true" ID="ID_1057053248" CREATED="1507235372146" MODIFIED="1507235426395">
<node ID="ID_1441461929" CREATED="1507208244261" MODIFIED="1507235308358"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="getdeck_sample.png"/>
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
</node>
<node TEXT="nombreux plugins, utiles ou de gamifications" ID="ID_1901565895" CREATED="1504614225643" MODIFIED="1507148224680"><richcontent TYPE="NOTE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      En python
    </p>
  </body>
</html>

</richcontent>
<node TEXT="accessibles sur site web" ID="ID_53592429" CREATED="1507145733633" MODIFIED="1507145748963"/>
<node TEXT="plugin list" FOLDED="true" ID="ID_202087454" CREATED="1507145635082" MODIFIED="1507145639308">
<node ID="ID_1028763855" CREATED="1507129082119" MODIFIED="1507145585984"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="plugin_list.png"/>
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="plugin detail" FOLDED="true" ID="ID_1930922175" CREATED="1507145647082" MODIFIED="1507145650548">
<node ID="ID_339651411" CREATED="1507129082119" MODIFIED="1507145534565"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="plugin_detail.png"/>
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="e.g. : &quot;incremental reading&quot;" ID="ID_429422158" CREATED="1507194453452" MODIFIED="1507204458862">
<node TEXT="M&#xe9;thode pour &#xe9;tudier, comprendre (et retenir) un ensemble de textes long et complexe" ID="ID_1522166438" CREATED="1507203612664" MODIFIED="1507204467606"/>
<node TEXT="importer un texte" ID="ID_619732191" CREATED="1507203635735" MODIFIED="1507203744271"/>
<node TEXT="travailler sur le texte :&#xa;- suppression parties inutiles&#xa;- extraire des blocs&#xa;- surligner des blocs&#xa;- en extraire des cartes" ID="ID_1379789091" CREATED="1507203744990" MODIFIED="1507234732399">
<node TEXT="phase de (re)lecture" ID="ID_798028664" CREATED="1507208199853" MODIFIED="1507235717232">
<node ID="ID_246398448" CREATED="1507208244261" MODIFIED="1507208276240"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="incremental_reading.png"/>
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="choix de sa prochaine r&#xe9;apparition" ID="ID_1768654982" CREATED="1507208214773" MODIFIED="1507235736262">
<node ID="ID_98297711" CREATED="1507208280228" MODIFIED="1507208310327"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="incremental_when.png"/>
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="ordonnancement" ID="ID_1872888867" CREATED="1507208234245" MODIFIED="1507208238486">
<node ID="ID_682598319" CREATED="1507208315292" MODIFIED="1507208347456"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="incremental_ordering.png"/>
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
</node>
<node TEXT="arr&#xea;ter quand vous le souhaitez et y revenir" ID="ID_418296008" CREATED="1507203890571" MODIFIED="1507204050863">
<node TEXT="raisons" ID="ID_556319294" CREATED="1507204482013" MODIFIED="1507204538546">
<attribute NAME="slide-hide" VALUE=""/>
<node TEXT="plus de temps" ID="ID_727557364" CREATED="1507204496389" MODIFIED="1507204542410"/>
<node TEXT="lass&#xe9;" ID="ID_1490613265" CREATED="1507204499309" MODIFIED="1507204504859"/>
</node>
</node>
</node>
</node>
</node>
</node>
<node TEXT="Concurrents" FOLDED="true" POSITION="right" ID="ID_1100728074" CREATED="1504615725466" MODIFIED="1507127983906">
<node TEXT="Quelques logiciels similaires :" ID="ID_122816406" CREATED="1507145794550" MODIFIED="1507145818021"/>
<node TEXT="supermemo" ID="ID_293568122" CREATED="1504615543607" MODIFIED="1504615548081">
<node TEXT="Anki est consid&#xe9;r&#xe9; comme une r&#xe9;&#xe9;criture de ce logiciel priv&#xe9;" ID="ID_1933500567" CREATED="1504615526008" MODIFIED="1507044834156"/>
<node TEXT="payant et veillissant" ID="ID_1783322205" CREATED="1504616602267" MODIFIED="1504616607054"/>
<node TEXT="https://www.supermemo.com" ID="ID_1468724512" CREATED="1507146203115" MODIFIED="1507146203115" LINK="https://www.supermemo.com"/>
</node>
<node TEXT="mnemosyne" ID="ID_328970439" CREATED="1504615756903" MODIFIED="1504615762515">
<node TEXT="https://mnemosyne-proj.org/" ID="ID_1303094411" CREATED="1504615754100" MODIFIED="1504615754100" LINK="https://mnemosyne-proj.org/"/>
<node TEXT="en parall&#xe8;le est utilis&#xe9; pour une &#xe9;tude sur la m&#xe9;moire humaine" ID="ID_757734136" CREATED="1504615930249" MODIFIED="1504616621117"/>
<node TEXT="depuis 2003" ID="ID_1460472030" CREATED="1504616012891" MODIFIED="1504616016471"/>
</node>
</node>
<node TEXT="Retour sur 1 an d&apos;utilisation" FOLDED="true" POSITION="right" ID="ID_1691571949" CREATED="1504616142990" MODIFIED="1507236277237">
<node ID="ID_1193428909" CREATED="1507148416914" MODIFIED="1507150072392"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      un tr&#232;s bon compl&#233;ments aux cartes mentales <img src="carte_mentale.jpg" width="266" height="154"/>
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="r&#xe9;ussi &#xe0; apprendre : les expressions r&#xe9;guli&#xe8;res, l&apos;orga d&apos;Orange, commandes git..." ID="ID_125325046" CREATED="1507045853019" MODIFIED="1507045903455"/>
<node TEXT="qques m&#xe9;triques" ID="ID_1629530841" CREATED="1507045383636" MODIFIED="1507149680253">
<node TEXT=" tir&#xe9;es du module stat" ID="ID_505477657" CREATED="1507149684579" MODIFIED="1507149687045"/>
<node TEXT="nombre de cartes" FOLDED="true" ID="ID_570559274" CREATED="1507149597596" MODIFIED="1507149601684">
<node ID="ID_1626563246" CREATED="1507149275701" MODIFIED="1507149332136"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="stats_oneyear_number.png"/>
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="rythme d&apos;addition" FOLDED="true" ID="ID_1408972048" CREATED="1507149603866" MODIFIED="1507149613684">
<node ID="ID_1607814424" CREATED="1507149275701" MODIFIED="1507149364196"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="stats_oneyear_addition.png"/>
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="temps pass&#xe9; &#xe0; r&#xe9;viser" FOLDED="true" ID="ID_1887622947" CREATED="1507149615979" MODIFIED="1507149627333">
<node ID="ID_457174447" CREATED="1507149275701" MODIFIED="1507149475105"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="stats_oneyear_time.png"/>
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="peu &#xe0; peu les cartes sont apprises" FOLDED="true" ID="ID_785117553" CREATED="1507149629940" MODIFIED="1507149640229">
<node ID="ID_1043639613" CREATED="1507149275701" MODIFIED="1507149513441"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <img src="stats_oneyear_interval.png"/>
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
</node>
<node TEXT="qques conseils :" ID="ID_1015795025" CREATED="1507044896709" MODIFIED="1507045197813">
<node TEXT="la r&#xe9;vision d&apos;une fiche doit prendre moins de 10 secondes (r&#xe9;flexion + lecture) :" ID="ID_1830017356" CREATED="1507045333132" MODIFIED="1507045354358"/>
<node TEXT="&#xe9;crivez vos fiches vous m&#xea;me, plut&#xf4;t qu&apos;en r&#xe9;utilisant ou en copiant-collant&#xa;elles seront plus concises et adapt&#xe9;es" ID="ID_1468576124" CREATED="1504616159527" MODIFIED="1507045376276"/>
<node TEXT="faire des fiches courtes (ou &#xe0; deux niveaux de lecture)" ID="ID_398422784" CREATED="1507045433108" MODIFIED="1507218471655"/>
<node TEXT="Supprimer (ou suspendre) les fiches devenues inutiles" ID="ID_987417861" CREATED="1506506603201" MODIFIED="1507045428293"/>
<node TEXT="jouez avec le d&#xe9;lai max suivant le paquet" ID="ID_1696867414" CREATED="1507149746696" MODIFIED="1507149764296"/>
</node>
</node>
<node TEXT="R&#xe9;f&#xe9;rences" FOLDED="true" POSITION="right" ID="ID_728695051" CREATED="1504615951008" MODIFIED="1507128040689">
<node TEXT="Cette pr&#xe9;sentation a &#xe9;t&#xe9; cr&#xe9;&#xe9; en utilisant : freemind + mindslide + reveal.js" ID="ID_1302774446" CREATED="1507144234054" MODIFIED="1507147191677"/>
<node TEXT="https://nihongoperapera.com/mnemosyne-anki-review.html" ID="ID_1657004296" CREATED="1504615955211" MODIFIED="1504615955211" LINK="https://nihongoperapera.com/mnemosyne-anki-review.html"/>
<node TEXT="Damien SCONTRINO, GREID acad&#xe9;mie de Cr&#xe9;teil&#xa;utilisation en tp pour r&#xe9;viser les cours&#xa;http://economie-gestion.ac-creteil.fr/spip.php?article448" ID="ID_1917380477" CREATED="1506284325847" MODIFIED="1507147109539"/>
</node>
<node TEXT="Lancez-vous" POSITION="right" ID="ID_1745161861" CREATED="1507129029192" MODIFIED="1507538859100">
<node TEXT="En t&#xe9;l&#xe9;chargeant cette pr&#xe9;sentation ou en allant sur le site d&apos;ankiweb" ID="ID_1421272711" CREATED="1507147090635" MODIFIED="1507538890957"/>
<node ID="ID_101881174" CREATED="1507146807904" MODIFIED="1507538847236" LINK="https://gitlab.com/nbossard/devday-anki"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      https://gitlab.com/nbossard/devday-anki
    </p>
    <p>
      <img src="qrcode.png"/>
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="https://apps.ankiweb.net/" ID="ID_1811172065" CREATED="1507538899941" MODIFIED="1507538899941" LINK="https://apps.ankiweb.net/"/>
</node>
<node TEXT="Retour pr&#xe9;sentations" POSITION="right" ID="ID_1105076453" CREATED="1507538149457" MODIFIED="1507538323713">
<attribute NAME="slide-hide" VALUE=""/>
<node TEXT="dev day&#xa;9 septembre 2017" ID="ID_278635125" CREATED="1507538364965" MODIFIED="1507538501547">
<node TEXT="confidentialit&#xe9; des donn&#xe9;es? possibilit&#xe9; d&apos;avoir son propre serveur?" ID="ID_141857216" CREATED="1507538218837" MODIFIED="1507538249758">
<node TEXT="" ID="ID_371248613" CREATED="1507538332173" MODIFIED="1507538332173"/>
</node>
<node TEXT="version iOS laquelle? la gratuite est elle ok ?" ID="ID_110229226" CREATED="1507538190590" MODIFIED="1507538211007"/>
<node TEXT="demande d&apos;une d&#xe9;mo" ID="ID_975712568" CREATED="1507538183086" MODIFIED="1507538189687">
<node TEXT="t&#xe9;l&#xe9;chargement d&apos;un paquet musique" ID="ID_866593258" CREATED="1507538264790" MODIFIED="1507538272350"/>
</node>
<node TEXT="y a t&apos;il une synchronisation entre devices (sic)?" ID="ID_905796988" CREATED="1507538276574" MODIFIED="1507538295583">
<node TEXT="sic" ID="ID_1517198418" CREATED="1507538299750" MODIFIED="1507538300855"/>
</node>
</node>
</node>
</node>
</map>
